package main

import "gitlab.com/carlosdanna/golang-basic-server/mvc/app"

func main() {
	app.StartApp()
}
