package services

import (
	"gitlab.com/carlosdanna/golang-basic-server/mvc/domain"
	"gitlab.com/carlosdanna/golang-basic-server/mvc/utils"
)

// These services could be calling third parties rest apis

func GetUser(userId int64) (*domain.User, *utils.ApplicationError) {
	return domain.GetUser(userId)
}
