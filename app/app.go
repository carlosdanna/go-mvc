package app

import (
	"net/http"

	"gitlab.com/carlosdanna/golang-basic-server/mvc/controllers"
)

func StartApp() {
	http.HandleFunc("/users", controllers.GetUser)
	if err := http.ListenAndServe(":8080", nil); err != nil {
		panic(err)
	}
}
