package controllers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"gitlab.com/carlosdanna/golang-basic-server/mvc/services"
	"gitlab.com/carlosdanna/golang-basic-server/mvc/utils"
)

func GetUser(resp http.ResponseWriter, req *http.Request) {
	userIdParam := req.URL.Query().Get("user_id")

	userId, err := (strconv.ParseInt(userIdParam, 10, 64))

	if err != nil {
		// Just return Bad Request to the Client
		userErr := &utils.ApplicationError{
			Message:    "User id must be a number",
			StatusCode: http.StatusBadRequest,
			Code:       "bad_request",
		}
		resp.WriteHeader(userErr.StatusCode)
		jsonValue, _ := json.Marshal(userErr)
		resp.Write(jsonValue)
		return
	}

	user, apiErr := services.GetUser(userId)

	if apiErr != nil {
		// Handle the err and return to the client
		resp.WriteHeader(apiErr.StatusCode)
		jsonValue, _ := json.Marshal(apiErr)
		resp.Write(jsonValue)
		return
	}

	// Return user to the client
	jsonValue, _ := json.Marshal(user)
	resp.Write(jsonValue)
}
