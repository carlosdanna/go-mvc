# Golang - MVC

This is code copied from tutorial I was following about making applications in Go

## Basic Project Structure

```
|
|-- app              Core configuration of the application
|-- controllers      Controller Logic
|-- domain           Domain information such as struct and database connection through dao
|-- services         Use for third party integrations
|-- utils            General purpose functions used throughout the application
|- main.go           Main function of the application
```

## Reference

https://www.udemy.com/course/golang-the-ultimate-guide-to-microservices-in-go-part-1/
